import React, { Component } from 'react';
import axios from 'axios';
import MUIDataTable from "mui-datatables";

class Users extends Component {
  constructor(props) {
    super(props)
    this.state = {
      users: []
    };
  }

  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/users')
    .then(res => {
      const users = res.data;
      this.setState({ users });
    })
  }

  render() {
    const columns = [
      {
        name: "name",
        label: "Name",
        options: {
          filter: true,
          sort: true,
        }
      },
      {
        name: "username",
        label: "User Name",
        options: {
          filter: true,
          sort: true,
        }
      },
      {
        name: "phone",
        label: "Phone",
        options: {
          filter: true,
          sort: true,
        }
      },
      {
        name: "email",
        label: "Email",
        options: {
          filter: true,
          sort: true,
        }
      },
      {
        name: "website",
        label: "Website",
        options: {
          filter: true,
          sort: true,
        }
      },
    ];

    return (
      <>
      <div className="container">
        <div className="py-5 text-center">
          <h2>Users</h2>
        </div>
      </div>
      <div className="album py-5 bg-light">
        <div className="container">
        <MUIDataTable
          title={"User List"}
          data={this.state.users}
          columns={columns}
        />
        </div>
      </div>
      </>
    );
  }
}

export default Users;
