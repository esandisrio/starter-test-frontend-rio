import React from 'react';
import { Redirect, Switch, Route } from 'react-router-dom';

import Home from './containers/Home/Home';
import Pricing from './containers/Pricing/Pricing';
import Checkout from './containers/Checkout/Checkout';
import Users from './containers/Users/Users';

function App() {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/pricing" component={Pricing} />
      <Route path="/checkout" component={Checkout} />
      <Route path="/users" component={Users} />
      <Redirect to="/" />
    </Switch>
  );
}

export default App;
